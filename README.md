# ERPNext ByteLab App
This repository contains ERPNext application with Byte Lab customisations.

## Installation
Installing application

```shell
bench get-app bytelab https://gitlab.com/byte-lab-erp/bytelab.git
bench --site erp.byte-lab.com install-app bytelab
bench --site erp.byte-lab.com migrate
```

### Git configuration 
By default, after application installation, only `mastar` branch is tracked. To be able to switch between different branches, all branches must be tracked. To enable this tracking, edit `frappe-bench/apps/bytelab/.git/config` and add following line: 

```shell
[remote "origin"]
    fetch = +refs/heads/*:refs/remotes/origin/*
```

## Updating
To update all installed applications run: 

```shell
bench update
```

