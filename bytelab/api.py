from __future__ import unicode_literals
import frappe
from frappe.utils import add_days, cint, cstr, flt, getdate, rounded, date_diff, money_in_words

from frappe import msgprint, _, throw, utils

@frappe.whitelist()
def get_whse_list_stock(item_code):
	records = frappe.db.sql("""select warehouse, sum(ledger.actual_qty) as stock_qty
		from `tabBin` ledger where ledger.item_code = %s GROUP BY ledger.warehouse HAVING stock_qty > 0 ORDER BY stock_qty desc""", item_code, as_dict=1)
	if records:
#		frappe.msgprint(_(records[0].warehouse))
#		return records[0].warehouse
		return records
	else:
		return

@frappe.whitelist()
def set_whse(item_code):
	records = frappe.db.sql("""select warehouse, sum(ledger.actual_qty) as stock_qty
		from `tabBin` ledger where ledger.item_code = %s GROUP BY ledger.warehouse HAVING stock_qty > 0 ORDER BY stock_qty desc""", item_code, as_dict=1)
	if records:
#		frappe.msgprint(_(records[0].warehouse))
		return records[0].warehouse
#		return records
	else:
		return

