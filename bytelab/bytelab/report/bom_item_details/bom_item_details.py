# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _, msgprint

def execute(filters=None):
#	if not filters: filters = {}
	global bom
	columns = get_columns()
	summ_data = []

	data = get_bom_stock(filters)

	for rows in data:
	        item_map = get_item_details(rows[0], filters)
		ctr = 1			
		if rows[4] > 0:
			whse_stock = get_item_stock_details(rows[0])
						
			if whse_stock:
				for wh_rows in whse_stock:
					if ctr == 1:
			        		summ_data.append([
        			                rows[0], item_map[rows[0]]["detail"], item_map[rows[0]]["reference"], rows[1], item_map[rows[0]]["manufacturer_part_no"], rows[2], wh_rows.warehouse, wh_rows.stock_qty])
					else:
						summ_data.append([
        			                "", "", "", "", "", "", wh_rows.warehouse, wh_rows.stock_qty])
					ctr = ctr + 1

		else:
			summ_data.append([
                        	rows[0], item_map[rows[0]]["detail"], item_map[rows[0]]["reference"], rows[1], item_map[rows[0]]["manufacturer_part_no"], rows[2], "", 0
                        
                    ])


	return columns, summ_data

def get_columns():
	"""return columns"""
	columns = [
		_("Item") + ":Link/Item:100",
		_("Item Detail") + "::150",
		_("Item Reference") + "::100",
		_("Description") + "::150",
		_("Manufacturer Part Number") + "::100",
		_("Required Qty") + ":Float:100",
		_("Warehouse") + "::100",
		_("Stock Qty") + ":Float:100",
	]

	return columns

def get_bom_stock(filters):
	conditions = ""
	bom = filters.get("bom")

	table = "`tabBOM Item`"
	qty_field = "qty"

	if filters.get("show_exploded_view"):
		table = "`tabBOM Explosion Item`"
		qty_field = "stock_qty"

	if filters.get("warehouse"):
		warehouse_details = frappe.db.get_value("Warehouse", filters.get("warehouse"), ["lft", "rgt"], as_dict=1)
		if warehouse_details:
			conditions += " and exists (select name from `tabWarehouse` wh \
				where wh.lft >= %s and wh.rgt <= %s and ledger.warehouse = wh.name)" % (warehouse_details.lft,
				warehouse_details.rgt)
		else:
			conditions += " and ledger.warehouse = '%s'" % frappe.db.escape(filters.get("warehouse"))

	else:
		conditions += ""

	return frappe.db.sql("""
			SELECT
				bom_item.item_code,
				bom_item.description, 
				bom_item.{qty_field},
				sum(ledger.actual_qty) as actual_qty,
				sum(FLOOR(ledger.actual_qty / bom_item.{qty_field}))as to_build
			FROM
				{table} AS bom_item
				LEFT JOIN `tabBin` AS ledger
				ON bom_item.item_code = ledger.item_code
				{conditions}
				
			WHERE
				bom_item.parent = '{bom}' and bom_item.parenttype='BOM'

			GROUP BY bom_item.item_code""".format(qty_field=qty_field, table=table, conditions=conditions, bom=bom))

def get_item_details(item_code, filters):
        condition = ''
        value = ()
	bom = filters.get("bom")

        items = frappe.db.sql("""select it.item_group, it.item_name, it.stock_uom, it.name, it.brand, it.description, it.manufacturer_part_no, it.detail, bomi.reference
                from tabItem it, `tabBOM Item` bomi where it.item_code = %s and bomi.parent = %s and bomi.item_code = it.item_code""", (item_code, bom), as_dict=1)
	if items:
		return dict((d.name, d) for d in items)
	else:

		items = frappe.db.sql("""select it.item_group, it.item_name, it.stock_uom, it.name, it.brand, it.description, it.manufacturer_part_no, it.detail, "" as reference
                from tabItem it where it.item_code = %s""", (item_code), as_dict=1)
		return dict((d.name, d) for d in items)


def get_item_stock_details(item_code):


	item_stock_details = frappe.db.sql("""select warehouse, sum(ledger.actual_qty) as stock_qty
		from `tabBin` ledger where ledger.item_code = %s GROUP BY ledger.warehouse HAVING stock_qty > 0""", item_code, as_dict=1)
	
	return item_stock_details


